package com.fetech.smart.validate.an;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
public @interface MaxLength {
    int value();

    String msg() default "";
}
